`define COLUMN_WIDTH 14             //Corresponds to No. of bytes needed in a page. Eg : 2^12 = 4kB  
`define PAGE_WIDTH 3                //Corresponds to No. of pages needed in a block. Eg : 2^6 = 64 Pages 
`define BLOCK_WIDTH 2               //Corresponds to No. of blocks needed in a PLANE(not in a LUN). Eg : 2^10 = 1024 blocks per plane 
`define PLANE_WIDTH 1               //Corresponds to No. of planes needed in a LUN. This s FIXED to 2^1 = 2.
`define LUN_WIDTH 1                 //Corresponds to No. of LUNS needed in a target chip. This s FIXED to 2^1 = 2.
`define TOTAL_PLANE 4               //#LUNS * #planes. Fixed
`define VALID_COL_ADDR 16383         //(2^COLUMN_WIDTH-1)+SPARE_BYTES_AREA(Say 64B)
`define WDC 128                      //NVMe - NFC Bus width
`define TOTAL_CHIPS 2               //Total chips per NFC
`define MAX_ROW_BITS 22             // This changes with different Flash Classes. Need to refer to nand_parameters.vh for this value

`define VALID_SPARE_AREA 1215       // Size of spare area in Bytes - 1, (Say 0 to 63B total of 64B) in whole page.
`define SECTOR_SPARE_SIZE 38         // Size of spare area for each sector.  (VALID_SPARE_AREA+1)*512/(2^COLUMN_WIDTH)

`define FIFO_ROWS 1023              //Page_size/WDC -1               
`define LBPR 4                      //Log Bytes Per Row. logbase2(WDC/8) .

//Based on above value comment out suitable things below.
//`define COLUMN_WIDTH_LT_8
//`define COLUMN_WIDTH_E_8
`define COLUMN_WIDTH_GT_8

`define PAGE_WIDTH_LT_8
//`define PAGE_WIDTH_E_8
//`define PAGE_WIDTH_GT_8

`define PAGE_PLANE_WIDTH_LT_8
//`define PAGE_PLANE_WIDTH_E_8
//`define PAGE_PLANE_WIDTH_GT_8

`define PAGE_PLANE_BLOCK_WIDTH_LTE_8
//`define PAGE_PLANE_BLOCK_WIDTH_GT_8

`define PAGE_PLANE_BLOCK_WIDTH_LTE_16
//`define PAGE_PLANE_BLOCK_WIDTH_GT_16

`define PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_8
//`define PAGE_PLANE_BLOCK_LUN_WIDTH_GT_8

`define PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_16
//`define PAGE_PLANE_BLOCK_LUN_WIDTH_GT_16


`define TIMING_MODE 5

//Choose CLK_PERIOD >= tWP*2
`define VCC_DELAY 10                 // ceil(tVCC/CLK_PERIOD). For simulation setting this value small but valid
`define TWP_MODE0 3                  // ceil(tWP mode0 / CLK_PERIOD)
`define TWB_MODE0 10                  // ceil(tWB mode0 / CLK_PERIOD)
`define TADL_MODE0 10                  // ceil(tADL mode0 / CLK_PERIOD)
`define TWB_COUNT 5                  // ceil(tWB/CLK_PERIOD)
`define TWHR_COUNT 3                 // ceil(tWHR/CLK_PERIOD)
`define TRHW_COUNT 5                 // ceil(tRHW/CLK_PERIOD)
`define TCCS_COUNT 15                 // ceil(tCCS/CLK_PERIOD)
`define TADL_COUNT 4                 // ceil(tADL/CLK_PERIOD)
