package NandFlashFunctBlockTargetTop ;
//Idea of this package is to hold 2 target chips as a single block

import InterfaceNandFlashFunctBlockTarget :: * ;
import NandFlashFunctBlockTarget :: * ;

interface ONFI_Target_Interface_top ;
	method Bit#(8) _data_to_nfc_0 ;
	method Action _data_from_nfc_0 ( Bit#(8) _data_from_nfc ) ;
	method Action _onfi_ce_n_0 ( bit _onfi_ce_n ) ;
	method Action _onfi_we_n_0 ( bit _onfi_we_n ) ;
	method Action _onfi_re_n_0 ( bit _onfi_re_n ) ;
	method Action _onfi_wp_n_0 ( bit _onfi_wp_n ) ;
	method Action _onfi_cle_0 ( bit _onfi_cle ) ;
	method Action _onfi_ale_0 ( bit _onfi_ale ) ;
	method bit t_ready_busy_n_0 ;
	
	method Bit#(8) _data_to_nfc_1 ;
	method Action _data_from_nfc_1 ( Bit#(8) _data_from_nfc ) ;
	method Action _onfi_ce_n_1 ( bit _onfi_ce_n ) ;
	method Action _onfi_we_n_1 ( bit _onfi_we_n ) ;
	method Action _onfi_re_n_1 ( bit _onfi_re_n ) ;
	method Action _onfi_wp_n_1 ( bit _onfi_wp_n ) ;
	method Action _onfi_cle_1 ( bit _onfi_cle ) ;
	method Action _onfi_ale_1 ( bit _onfi_ale ) ;
	method bit t_ready_busy_n_1 ;
endinterface

(*synthesize*)
module mkNandFlashTargetTop ( ONFI_Target_Interface_top ) ;
	
	ONFI_Target_Interface target_chip0 <- mkNandFlashTarget() ;
	ONFI_Target_Interface target_chip1 <- mkNandFlashTarget() ;
		
	method Bit#(8) _data_to_nfc_0 ;
		return target_chip0.onfi_target_interface._data_to_nfc_m();
	endmethod
	
	method Bit#(8) _data_to_nfc_1 ;
		return target_chip1.onfi_target_interface._data_to_nfc_m();
	endmethod
	
	method Action _data_from_nfc_0 ( data_from_nfc ) ;
		target_chip0.onfi_target_interface._data_from_nfc_m(data_from_nfc);
	endmethod
	
	method Action _data_from_nfc_1 ( data_from_nfc ) ;
		target_chip1.onfi_target_interface._data_from_nfc_m(data_from_nfc);
	endmethod
	
	method Action _onfi_ce_n_0 ( onfi_ce_n ) ;
		target_chip0.onfi_target_interface._onfi_ce_n_m(onfi_ce_n);
	endmethod
	
	method Action _onfi_ce_n_1 ( onfi_ce_n ) ;
		target_chip1.onfi_target_interface._onfi_ce_n_m(onfi_ce_n);
	endmethod
	
	method Action _onfi_we_n_0 ( onfi_we_n ) ;
		target_chip0.onfi_target_interface._onfi_we_n_m(onfi_we_n);
	endmethod
	
	method Action _onfi_we_n_1 ( onfi_we_n ) ;
		target_chip1.onfi_target_interface._onfi_we_n_m(onfi_we_n);
	endmethod
	
	method Action _onfi_re_n_0 ( onfi_re_n ) ;
		target_chip0.onfi_target_interface._onfi_re_n_m(onfi_re_n);
	endmethod
	
	method Action _onfi_re_n_1 ( onfi_re_n ) ;
		target_chip1.onfi_target_interface._onfi_re_n_m(onfi_re_n);
	endmethod
	
	method Action _onfi_wp_n_0 ( onfi_wp_n ) ;
		target_chip0.onfi_target_interface._onfi_wp_n_m(onfi_wp_n);
	endmethod
	
	method Action _onfi_wp_n_1 ( onfi_wp_n ) ;
		target_chip1.onfi_target_interface._onfi_wp_n_m(onfi_wp_n);
	endmethod
	
	method Action _onfi_cle_0 ( onfi_cle ) ;
		target_chip0.onfi_target_interface._onfi_cle_m(onfi_cle);
	endmethod
	
	method Action _onfi_cle_1 ( onfi_cle ) ;
		target_chip1.onfi_target_interface._onfi_cle_m(onfi_cle);
	endmethod
	
	method Action _onfi_ale_0 ( onfi_ale ) ;
		target_chip0.onfi_target_interface._onfi_ale_m(onfi_ale);
	endmethod
	
	method Action _onfi_ale_1 ( onfi_ale ) ;
		target_chip1.onfi_target_interface._onfi_ale_m(onfi_ale);
	endmethod
	
	method bit t_ready_busy_n_0 ;
		return target_chip0.onfi_target_interface.t_ready_busy_n_();
	endmethod
	
	method bit t_ready_busy_n_1 ;
		return target_chip1.onfi_target_interface.t_ready_busy_n_();
	endmethod
endmodule
endpackage
 
