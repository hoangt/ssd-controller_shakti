/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Testbench for NVM Express Controller
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu

Often used abbreviations:
	PRP			:	Physical Region Page
	Q			:	Queue
	AQ			:	Admin Queue
	SQ			:	Submission Queue
	CQ			:	Completion Queue
	ASQ			:	Admin Submission Queue
	ACQ			:	Admin Completion Queue
	dw, dWord	:	Double Word
	sqid		:	Submission Queue ID
	cqid		:	completion Queue ID
*/

/*
Memory Map of simulated main memory:
	0x00000|000 - 0x00000|FFF		Admin Submission Queue Base Address
	0x00001|000 - 0x00001|FFF		Admin Completion Queue Base Address
	0x00002|000 - 0x00002|FFF		1. IO Submission Queue Base Address
	0x00003|000 - 0x00003|FFF		1. IO Completion Queue Base Address
	0x00004|000 - 0x00004|FFF		2. IO Submission Queue Base Address
	0x00005|000 - 0x00005|FFF		2. IO Completion Queue Base Address
	0x00006|000 - 0x00006|FFF		3. IO Submission Queue Base Address
	0x00007|000 - 0x00007|FFF		3. IO Completion Queue Base Address
	0x00008|000 - 0x00008|FFF		4. IO Submission Queue Base Address
	0x00009|000 - 0x00009|FFF		4. IO Completion Queue Base Address
	0x0000A|000 - 0x0000A|FFF		5. IO Submission Queue Base Address
	0x0000B|000 - 0x0000B|FFF		5. IO Completion Queue Base Address
	0x0000C|000 - 0x0000F|FFF		Reserved for more queues
	0x00010|000 - 0x0001F|FFF		used for data transmission (PRP)
*/

/*
memory page size 4096 bytes (of one PRP)  should be the same in
main_memory_model
*/
`define MPS 0
`define QSIZE 5  // five entries in all queues (admin as well as io)
// number of I/O completion queues // TODO not used everywhere
`define NO_IO_CQs 1
/*
number of I/O submission queues // TODO not used everywhere
should be the same in main_memory_controller
*/
`define NO_IO_SQs 1
// number of namespaces TODO not used everywhere; test feature NN
`define NO_NSPACES 5
`define SQ_ENTRY_SIZE 64  // queue entry size in bytes for all submission queues
`define CQ_ENTRY_SIZE 16  // queue entry size in bytes for all completion queues

`define DEBUG 0

package top_nvm_controller;

import Connectable::*;
import StmtFSM::*;
import Vector::*;

import NvmController::*;
import global_definitions::*;
import ddr_connection::*;

interface Ifc_top_nvm_controller;
	interface Ifc_read_data_ddr ifc_read_data_ddr;
	interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	interface Ifc_write_data_ddr ifc_write_data_ddr;
	interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method Action _config_ctrl(Bit#(32) _data);
	method Action _config_addr(Bit#(32) _address);
	method Action _config_data_in(Bit#(64) _data);
	method Bit#(64) config_data_out_();
	method Bit#(8) leds_();
endinterface


(* synthesize *)
(* always_ready *)
(* always_enabled *)
module mkTop_nvm_controller(Ifc_top_nvm_controller);

Ifc_Controller lv_nvm_controller <- mkNvmController;
Ifc_ddr_connection lv_ddr_connection <- mkDdr_connection;


/* Signals for configuration interface. */
Wire#(Bit#(32)) dwr_in_config_addr <- mkDWire(0);
Wire#(Bit#(64)) dwr_in_config_data <- mkDWire(0);
Reg#(bit) rg_last_config_ctrl <- mkReg(1'b0);


/* renamings for better readability */
let lv_nvm_config_ifc = lv_nvm_controller.ifc_config;
let lv_nvm_completion_ifc = lv_nvm_controller.ifc_completion;
let lv_nvm_to_pci_ifc = lv_nvm_controller.nvmTransmitToPCIe_interface;
let lv_nvm_interrupt_ifc = lv_nvm_controller.nvmInterruptSideA_interface;
let lv_nvm_nand_flash_ifc = lv_nvm_controller.ifc_nand_flash[0];
let lv_main_memory_model = lv_ddr_connection.ifc_main_memory_model;
let lv_nand_flash_model = lv_ddr_connection.ifc_nand_flash_model;
let lv_read_data_ddr = lv_ddr_connection.ifc_read_data_ddr;
let lv_read_cmd_ddr = lv_ddr_connection.ifc_read_cmd_ddr;
let lv_write_data_ddr = lv_ddr_connection.ifc_write_data_ddr;
let lv_write_cmd_ddr = lv_ddr_connection.ifc_write_cmd_ddr;
let lv_write_sts_ddr = lv_ddr_connection.ifc_write_sts_ddr;




/* Make connection between lv_nand_flash_model and lv_nvm_nand_flash_ifc */
rule rl_connect_nand_request_data;
	let lv_tmp <- lv_nvm_nand_flash_ifc.request_address_();
	lv_nand_flash_model._request_data(
		truncate(lv_tmp.address),
		unpack(truncate(lv_tmp.length))
	);
endrule: rl_connect_nand_request_data

rule rl_connect_nand_data_in;
	let lv_tmp <- lv_nand_flash_model._get_data_();
	lv_nvm_nand_flash_ifc._data_in(lv_tmp);
endrule: rl_connect_nand_data_in

rule rl_connect_nand_write;
	$display("%d: Testbench: nand flash write.", $stime());
	let lv_tmp <- lv_nvm_nand_flash_ifc.data_out_();
	lv_nand_flash_model._write(
		truncate(lv_tmp.address),
		lv_tmp.data,
		truncate(lv_tmp.length)
	);
endrule: rl_connect_nand_write

rule rl_connect_enable;
	lv_nand_flash_model._enable(~lv_nvm_nand_flash_ifc._enable());
endrule: rl_connect_enable

rule rl_connect_interrupt;
	if (lv_nand_flash_model.interrupt_() == 1'b1)
		lv_nvm_nand_flash_ifc._interrupt();
endrule: rl_connect_interrupt

rule rl_connect_busy;
	lv_nvm_nand_flash_ifc._busy(lv_nand_flash_model.busy_());
endrule: rl_connect_busy





/* Make connection between main_memory_model and lv_nvm_to_pci_ifc and
lv_nvm_completion_ifc. */
Reg#(Bit#(16)) rg_connect_nvm_tag <- mkRegU();

rule rl_connect_nvm_main_mem_ready;
	if (lv_nvm_to_pci_ifc.write_() == 1'b0) begin
		lv_nvm_to_pci_ifc._wait(
			~lv_main_memory_model.nvm_request_data_ready_());
	//$display("%d: Testbench: PCIe ready: %d", $stime(),
	//	lv_main_memory_model.nvm_request_data_ready_());
	end else begin
		lv_nvm_to_pci_ifc._wait(
			~lv_main_memory_model.nvm_put_data_ready_());
	end
endrule: rl_connect_nvm_main_mem_ready

rule rl_connect_nvm_main_mem_read_req(
		lv_nvm_to_pci_ifc.data_valid_() == 1'b1 &&
		lv_nvm_to_pci_ifc.write_() == 1'b0
	);
	lv_main_memory_model._nvm_request_data(
		lv_nvm_to_pci_ifc.address_(),
		unpack(lv_nvm_to_pci_ifc.payload_length_())
	);
	rg_connect_nvm_tag <= lv_nvm_to_pci_ifc.tag_();
endrule: rl_connect_nvm_main_mem_read_req

rule rl_connect_nvm_main_mem_write_req(
		lv_nvm_to_pci_ifc.data_valid_() == 1'b1 &&
		lv_nvm_to_pci_ifc.write_() == 1'b1
	);
	lv_main_memory_model._nvm_put_data(
		lv_nvm_to_pci_ifc.data_(),
		lv_nvm_to_pci_ifc.address_(),
		unpack(extend(lv_nvm_to_pci_ifc.payload_length_()))
	);
endrule: rl_connect_nvm_main_mem_write_req

rule rl_connect_nvm_main_mem_get;
	let lv_data <- lv_main_memory_model._nvm_get_data_();
	//$display("%d: Get data for NVM from Main Memory. 0b%b",
	//	$stime(), lv_data);
	lv_nvm_completion_ifc._write(lv_data, rg_connect_nvm_tag);
endrule: rl_connect_nvm_main_mem_get




interface Ifc_read_data_ddr ifc_read_data_ddr;
	method bit ready_();
		return lv_read_data_ddr.ready_();
	endmethod: ready_

	method Action _valid(bit _is_valid);
		lv_read_data_ddr._valid(_is_valid);
	endmethod: _valid

	method Action _data_in(Bit#(32) _data);
		lv_read_data_ddr._data_in(_data);
	endmethod: _data_in

	method Action _last(bit _is_last);
		lv_read_data_ddr._last(_is_last);
	endmethod: _last
endinterface: ifc_read_data_ddr


interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	method bit valid_();
		return lv_read_cmd_ddr.valid_();
	endmethod: valid_

	method Bit#(72) data_();
		return lv_read_cmd_ddr.data_();
	endmethod: data_

	method Action _ready(bit _is_ready);
		lv_read_cmd_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_read_cmd_ddr


interface Ifc_write_data_ddr ifc_write_data_ddr;
	method bit valid_();
		return lv_write_data_ddr.valid_();
	endmethod: valid_

	method Bit#(32) data_out_();
		return lv_write_data_ddr.data_out_();
	endmethod: data_out_

	method bit last_();
		return lv_write_data_ddr.last_();
	endmethod: last_

	method Action _ready(bit _is_ready);
		lv_write_data_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_write_data_ddr


interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	method bit valid_();
		return lv_write_cmd_ddr.valid_();
	endmethod: valid_

	method Bit#(72) data_();
		return lv_write_cmd_ddr.data_();
	endmethod: data_

	method Action _ready(bit _is_ready);
		lv_write_cmd_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_write_cmd_ddr


interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method bit ready_();
		return lv_write_sts_ddr.ready_();
	endmethod: ready_

	method Action _valid(bit _is_valid);
		lv_write_sts_ddr._valid(_is_valid);
	endmethod: _valid

	method Action _data_in(Bit#(8) _data);
		lv_write_sts_ddr._data_in(_data);
	endmethod: _data_in
endinterface: ifc_write_sts_ddr


method Action _config_ctrl(Bit#(32) _data);
	if (_data[0] == ~rg_last_config_ctrl) begin
		lv_nvm_config_ifc._write(dwr_in_config_addr, dwr_in_config_data, False);
		rg_last_config_ctrl <= _data[0];
	end
endmethod: _config_ctrl

method Action _config_addr(Bit#(32) _address);
	dwr_in_config_addr <= _address;
endmethod: _config_addr

method Action _config_data_in(Bit#(64) _data);
	dwr_in_config_data <= _data;
endmethod: _config_data_in

method Bit#(64) config_data_out_();
	return lv_nvm_config_ifc.read_(dwr_in_config_addr);
endmethod: config_data_out_

method Bit#(8) leds_();
	/* Drive Board-LEDs. */
	return lv_nvm_controller.leds_();
endmethod: leds_


endmodule: mkTop_nvm_controller
endpackage: top_nvm_controller
